//
//  CollectionViewCell.swift
//  HallNBA
//
//  Created by Geoffroy De Biasi on 17/01/2020.
//  Copyright © 2020 Geoffroy de Biasi. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var thumbnail: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
