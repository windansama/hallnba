//
//  TeamDetailsViewController.swift
//  HallNBA
//
//  Created by Geoffroy De Biasi on 24/01/2020.
//  Copyright © 2020 Geoffroy de Biasi. All rights reserved.
//

import UIKit

class TeamDetailsViewController: UIViewController {

    var teamIdentifier: String?
    
    @IBOutlet var teamInformations: [UILabel]!
    @IBOutlet weak var teamImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let nameWithoutPrefix = teamIdentifier?.split(separator: "-")[1]
        
        let finalIdentifier = nameWithoutPrefix?.split(separator: ".")[0]
        let selectedTeam: Team? = NBALeague.findWithName(finalIdentifier!.description)
        if(selectedTeam != nil)
        {
            teamImage.image = UIImage(named: "TeamsIcons.bundle/" + teamIdentifier!)
            teamInformations[0].text = "Nom : " + selectedTeam!.name
            teamInformations[1].text = "Ville : " + selectedTeam!.city
            teamInformations[2].text = "Abréviation : " + selectedTeam!.abbreviation
            teamInformations[3].text = "Conférence : " + selectedTeam!.conference
            teamInformations[4].text = "Division : " + selectedTeam!.division
        }
    }
}
