//
//  ViewController.swift
//  HallNBA
//
//  Created by Geoffroy De Biasi on 17/01/2020.
//  Copyright © 2020 Geoffroy de Biasi. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDataSource {
        
    var teamsIcons = [String]()    
    let cell_identifier = "teamCell"
    let details_identifier = "teamDetails"

    
    @IBOutlet weak var teamsCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let fm = FileManager.default
        let path = Bundle.main.resourcePath! + "/TeamsIcons.bundle"
        let items = try! fm.contentsOfDirectory(atPath: path)
        for item in items {
            if item.hasPrefix("team-") {
                teamsIcons.append(item)
            }
        }
        teamsIcons.sort()

        teamsCollectionView.dataSource = self
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print(teamsIcons.count)
        return teamsIcons.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
                
        let cell = teamsCollectionView.dequeueReusableCell(withReuseIdentifier: cell_identifier, for: indexPath) as! CollectionViewCell

        let row = indexPath.row
        cell.thumbnail.image = UIImage(named: "TeamsIcons.bundle/" + teamsIcons[row])

        return cell
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let detailViewController = segue.destination as! TeamDetailsViewController
        
        let indexPath = self.teamsCollectionView.indexPathsForSelectedItems!
        
        let indexCell = indexPath[0][1]
        
        detailViewController.teamIdentifier = teamsIcons[indexCell]
    }

}

