//
//  Team.swift
//  HallNBA
//
//  Created by Geoffroy De Biasi on 24/01/2020.
//  Copyright © 2020 Geoffroy de Biasi. All rights reserved.
//

import Foundation

class Team{
    
    var name: String = ""
    var conference: String = ""
    var division: String = ""
    var city: String = ""
    var abbreviation: String = ""
}
