//
//  NBALeague.swift
//  HallNBA
//
//  Created by Geoffroy De Biasi on 27/01/2020.
//  Copyright © 2020 Geoffroy de Biasi. All rights reserved.
//

import Foundation

class NBALeague{
    
    static var teams = Array<Team>()
    
    static func initWithApi()
    {
        let headers = [
            "x-rapidapi-host": "free-nba.p.rapidapi.com",
            "x-rapidapi-key": "e6a3cd7d76msh765c6682c75bf28p1837f9jsnb9968c9116e3"
        ]

        let request = NSMutableURLRequest(url: NSURL(string: "https://free-nba.p.rapidapi.com/teams?page=0")! as URL,
                                                cachePolicy: .useProtocolCachePolicy,
                                            timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error!)
            }
            else {
                do{
                    let json = try JSONSerialization.jsonObject(with: data!, options: [])
                    let jsonData = json as! [String: Any]
                    let jsonArray = jsonData["data"] as! NSArray
                    
                    for item in jsonArray as! [Dictionary<String, AnyObject>] {
                        let team: Team = Team()
                        team.name = item["name"] as! String
                        team.division = item["division"] as! String
                        team.conference = item["conference"] as! String
                        team.abbreviation = item["abbreviation"] as! String
                        team.city = item["city"] as! String
                        self.teams.append(team)
                    }
                } catch {
                    print(error)
                }
            }
        })
        dataTask.resume()
    }
    
    static func findWithName(_ name: String) -> Team? {
        for team in self.teams
        {
            if(team.name.lowercased() == name.lowercased())
            {
                return team
            }
        }
        return nil
    }
}
